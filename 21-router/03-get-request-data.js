#!/usr/bin/env node


const Koa = require('koa');

const app = new Koa(),
      Router = require('koa-router'),
      router = new Router,
      log = console.log;
router.get('./api/tasks/',async(ctx,next)=>{
  let {page , limit} = ctx.query;
  log(`得到的待办事项,page = ${page},limit = ${limit}`);
  ctx.body = 'tianjiachenggong';
})
router.put('./api/tasks/',async(ctx,next)=>{
  let {id}=ctx.params;
  let {task} =ctx.request.body;
  log(`id=${id}, task = ${task}`);
  ctx.body = 'tianjiachenggong';
})
router.delete('./api/tasks/',async(ctx,next)=>{
  let {id} = ctx.params;
  log(`${id = ${id}}`);
  ctx.body = 'tianjiachenggong';
})
router.post('./api/tasks/',async(ctx,next)=>{
  let {task} = ctx.request.body;
  log(`task:${task}`);
  ctx.body = 'tianjiachenggong';
})

app.use();

app.listen(8080);

