-- 创建数据库
DROP DATABASE IF EXISTS `todo_list`;
CREATE DATABASE `todo_list` DEFAULT CHARACTER SET utf8mb4 ;

-- 创建表
DROP TABLE IF EXISTS `todo_list.users`;
CREATE TABLE `todo_list`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nickname` VARCHAR(255) NULL,
  `email` VARCHAR(255) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;

DROP TABLE IF EXISTS `todo_list.tasks`;
CREATE TABLE `todo_list`.`tasks` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `content` VARCHAR(255) NOT NULL,
  `user_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `user_id` (`user_id` ASC),
  CONSTRAINT `user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `todo_list`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;

-- 添加初始数据
USE todo_list;

INSERT INTO users (nickname, email, `password`)
VALUES('wangding', '408542507@qq.com', '123'), ('zhangsan', 'abc@163.com', 'abc');

INSERT INTO tasks(content, user_id)
VALUES('eat', 1), ('sleep', 1), ('shop', 2);