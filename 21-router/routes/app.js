#!/usr/bin/env node


const Koa = require('koa');

const Router = require('koa-router'),
      router = new Router,
      log = console.log;
router.prefix ('/api/tasks');
router.get('/',async(ctx,next)=>{
  let {page , limit} = ctx.query;
  log(`得到的待办事项,page = ${page},limit = ${limit}`);
  ctx.body = 'tianjiachenggong';
})
router.put('/',async(ctx,next)=>{
  let {id}=ctx.params;
  let {task} =ctx.request.body;
  log(`id=${id}, task = ${task}`);
  ctx.body = 'tianjiachenggong';
})
router.delete('/',async(ctx,next)=>{
  let {id} = ctx.params;
  log(`${id = ${id}}`);
  ctx.body = 'tianjiachenggong';
})
router.post('/',async(ctx,next)=>{
  let {task} = ctx.request.body;
  log(`task:${task}`);
  ctx.body = 'tianjiachenggong';
})

module.exports = routers;
