#!/usr/bin/node


let items=[];

const http = require('http'),
      url = require('url'),
      qs = require('querystring'),
      log = console.log;

function genPage(){
 let  html = `<!DOCTYPE html>
     <html lang="en">
       <head>
           <meta charset="UTF-8">
          <title>TODO LIST</title>
       </head>
            <body>
             <h1>TODOLIST</h1>
                <form method = "POST" action="http://192.168.22.144:8080">
                    <input type="text" name = "item">                                                                        <input type="submit" value="submit">
                </form>
                <ul id = "items">
                ${items.map(i =>'<li>'+i+'</li>').join('\n')}
                </ul>
             </body>
    </html>`;
    return html;
}
http.createServer((req,res)=>{
  if(req.url !=='/'){
    res.statusCode=404;
    res.setHeader('Content-Type','text/html');
    res.end('<h1>not found</h1>');
    return ;
  }
  let data = '';
  req.on('data',chunk =>data+=chunk);
  req.on('end',()=>{
    let item = qs.parse(data).item;
    if (item !==''&&typeof item !=='undefined') items.push(item);
  res.writeHead(200,{
  'Content-Type':'text/html',
  'Content-Length':Buffer.byteLength(genPage())
  });
   res.end(genPage());  
})
}).listen(8080);
