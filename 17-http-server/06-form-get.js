#!/usr/bin/node


let items=[];

const http = require('http'),
      url = require('url'),
      qs = require('querystring'),
    log = console.log;

function genPage(){
  log(items.length);
let  html = `<!DOCTYPE html>
     <html lang="en">
       <head>
           <meta charset="UTF-8">
          <title>TODO LIST</title>
       </head>
            <body>
             <h1>TODOLIST</h1>
                <form method = "GET" action="http://192.168.22.144:8080">
                    <input type="text" name = "item">                                                                        <input type="submit" value="submit">
                </form>
                <ul id = "items">
                ${items.map(i =>'<li>'+i+'</li>').join('\n')}
                </ul>
             </body>
    </html>`;
    return html;
}
http.createServer((req,res)=>{
  let addr = url.parse(req.url);
  if(addr.pathname !=='/'){
    res.statusCode=404;
    res.setHeader('Content-Type','text/html');
    res.end('<h1>not found</h1>');
    return ;
  }

  let item = qs.parse(addr.query).item;
  if (item !==''&&typeof item !=='undefined') items.push(item);

  res.writeHead(200,{
  'Content-Type':'text/html',
  'Content-Length':Buffer.byteLength(genPage())
  });
   res.end(genPage());
}).listen(8080);
