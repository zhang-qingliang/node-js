#!/usr/bin/node

const http = require('http');
const server = http.createServer();

server.on('request',(req,res)=>{
  switch(req.method){
    case 'GET':
      res.end('get method');
      break;
    case 'POST':
      res.end('post method');
       break;
    case 'DELETE':
      res.end('delete method');
       break;
    case 'PUT':
      res.end('put method');
       break;
    default :
      res.end(`${req.method} method`);
    
  }
});

server.listen(8080);
