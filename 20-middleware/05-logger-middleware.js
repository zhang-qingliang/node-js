#!/usr/bin/env node


const Koa = require('koa');

const app = new Koa();
app.use((ctx,next)=>{
  console.log(`${ctx.method} ${ctx.path}`);
  next(); 
});


app.use((ctx , next) =>{
   ctx.body = 'hello koa';
});

app.listen(1234);

