
#!/usr/bin/env node

function fakeAsync(){
  let delay =Math.random()*1000;
  return new Promise((resolve,reject)=>{
  setTimeout(()=>{
    console.log(`Async Task use ${delay}ms`);
  },delay);
})
}

console.log('start');

//module.exports = fakeAsync;
(async()=>{
  await fakeAsync();
})();
