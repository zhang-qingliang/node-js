#!/usr/bin/env node


const Koa = require('koa');

const app = new Koa(),
      log = console.log;
app.use((ctx,next)=>{
  console.log(`${ctx.method} ${ctx.path}`);
  next(); 
  log(`cost:${ctx.cost}ms`);
});
app.use(async(ctx,next)=>{
  const start = Date.now();
  next();
  const end = Date.now();
  ctx.cost = end-start;
  })

app.use((ctx , next) =>{
   ctx.body = 'hello koa';
});

app.listen(1234);

