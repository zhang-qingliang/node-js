#!/usr/bin/node

const log = console.log;

log(`${process.arch}`);
log(`${process.platform}`);
log(`${process.pid}`);
log(`${process.execPath}`);

log(`${process.version}`);
log(`${process.getuid()}`);
log(`${process.getgid()}`);

log(`${process.cwd()}`);

log(`env:`);
log(process.env);
log(`${process.env.HOSTNAME}`);
