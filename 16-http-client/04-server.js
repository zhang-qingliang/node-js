#!/usr/bin/node


const http = require('http'),
      log = console.log;

const server = http.createServer();
server.on('request',(req,res)=>{
  log(`${req.method} ${req.url} HTTP/${req.httpVersion}`);

  log(req.headers);

  req.pipe(process.stdout);

  res.end('ok!');
});

server.listen(8080);
