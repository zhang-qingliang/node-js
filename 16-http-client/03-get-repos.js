#!/usr/bin/env node

const http = require('https'),
            log  = console.log,
                  url  = require('url'),
                        name = process.argv[2];

let addr = `https://api.github.com/search/repositories?q=user:${name}`;

let opt = url.parse(addr);

opt.headers = {
    'user-agent':'curl/7.29.0'

};
log(opt);
process.exit();

http.get(addr,opt, (res) => {
    log(`HTTP/${res.httpVersion} ${res.statusCode} ${res.statusMessage}`);

      log(res.headers);

        let data = '';

          res.on('data',(chunk) => data += chunk);
          res.on('end', () => {
                let rs = [],num = 1;
                    data = JSON.parse(data).items;

                    data.forEach((item) => rs.push({
                            num: num++,
                            name:item.name,
                            description:item.description
                          
                    }));

                        
                        console.table(rs);
                          
          });

});

