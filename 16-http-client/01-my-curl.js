#!/usr/bin/env node


const http =  require('http'),
      log = console.log,
      url = require('url'),
      addr = process.argv[2];

http.get(addr , (res) => {
  //printf response start line
  //http/1.1 200 ok
  log(`HTTP/${res.httpVersion
  } ${res.statusCode} ${res.statusMessage}`);
  //printfresponse headers
  log(res.headers);
  res.pipe(process.stdout);

  let data = '';
  res.on('data',(chunck)=>{
    data += chunck;
  });
  res.on('end',()=>{
    log(data);
  });
});
