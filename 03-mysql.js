#!/usr/bin/env node

const mysql = require('mysql2/promise'),
      log   = console.log;

let con = mysql.createPool({
  host:'127.0.0.1',
  user:'root',
  password:'ddd',
  database:'test'
});

getAreas();

async function getAreas(){
  const sql = 'select * from areas';

  let [rows] = await con.execute(sql);
  return rows;

}

async function addAreas(areaName){
  const sql = `insert into areas(area_name) values('${areaName}')`;

  let [rows] = await con.execute(sql);
  return rows;

}

async function delAreas(areaName){
  const sql = `delete from areas where area_name = '${areaName}'`;

  let [rows] = await con.execute(sql);
  return rows;

}

async function updateAreas(oldName,newName){
  const sql = `update areas set area_name = '${newName}' where area_name = '${oldName}'`;

  let [rows] = await con.execute(sql);
  return rows;

}
(async function(){
  //await addAreas('英国');
  //await updateAreas('英国','大不列颠');
  await delAreas('大不列颠');
  log(await getAreas());
})();
